$(function() {

    //切换登录和注册时的盒子
    $('#quzhuce').on('click', function() {
        $('.login').hide();
        $('.reg').show();

    });

    $('#qudenglu').on('click', function() {

        $('.reg').hide();
        $('.login').show();
    })

    //使用layui内置模块的步骤
    //加载模块
    var form= layui.form;

    // 使用layui的表单验证功能

    form.verify({

        //各种验证规则

        //规则名称：['验证的代码'，'错误提示']

        pass : [/^[\S]{6,12}$/,'密码的长度必须是6到12位'],
        repwd: function(value) {

           var pwd = $('.password').val().trim();
           if(pwd != value) {

            return '两次密码不一致';
           }

        }
        // username: function(value, item){ //value：表单的值、item：表单的DOM对象
        //   if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
        //     return '用户名不能有特殊字符';
        //   }
        //   if(/(^\_)|(\__)|(\_+$)/.test(value)){
        //     return '用户名首尾不能出现下划线\'_\'';
        //   }
        //   if(/^\d+\d+\d$/.test(value)){
        //     return '用户名不能全为数字';
        //   }
        // }
        
        // //我们既支持上述函数式的方式，也支持下述数组的形式
        // //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
        // ,pass: [
        //   /^[\S]{6,12}$/
        //   ,'密码必须6到12位，且不能出现空格'
        // ] 
      });

      //加载弹出层

      var layer = layui.layer;

      //完成注册功能

      $('#reg-form').on('submit',function(e) {
        e.preventDefault();

        var data = $(this).serialize();

        console.log(data);
        //按照接口要求提交数据
        $.ajax({
            type : 'POST',
            url : 'http://www.liulongbin.top:3007/api/reguser',
            data : data,
            success : function(res) {
            console.log(res);

            if(res.status == 1) {

                return layer.msg(res.message);

            }
            //成功
            // alert('注册成功');

            layer.msg('注册成功');
            $('#qudenglu').click();
            }

        })

      })

      //完成登陆功能
})